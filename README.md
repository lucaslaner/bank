**Bank**

É um programa de menu para simular um banco 

**Iniciando**

Ao iniciar do programa o usuario escolhera uma das opções do menu 

**Atualizações a serem feitas**

As principais atualizações a serem feitas são a implementação de: 

1.  **Pix**
    - Receber ou enviar o dinheiro;
    - A chave da pessoa para que vai mandar o pix; 
    - O valor que vai receber ou enviar;
    - Mostrar novo saldo da conta;
    
2.  **Pagamento**
    - Codigo da conta a ser paga;
    - Mostrar novo saldo da conta;

3.  **Investimento**
    - Valor a ser investido;
    - Quanto recebeu pelo investimento;
    - Mostrar novo saldo da conta;
